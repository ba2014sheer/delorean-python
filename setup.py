#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import delorean.delorean

# Ceci n'est qu'un appel de fonction. Mais il est trèèèèèèèèèèès long
# et il comporte beaucoup de paramètres
setup(
    # le nom de votre bibliothèque, tel qu'il apparaitre sur pypi
    name="delorean",
    # la version du code
    version=delorean.delorean.__version__,
    # Liste les packages à insérer dans la distribution
    # plutôt que de le faire à la main, on utilise la foncton
    # find_packages() de setuptools qui va cherche tous les packages
    # python recursivement dans le dossier courant.
    # C'est pour cette raison que l'on a tout mis dans un seul dossier:
    # on peut ainsi utiliser cette fonction facilement
    packages=find_packages(),
    # votre pti nom
    author="Nicolas Rouviere",
    author_email="zesk06@gmail.com",
    description="Nom de Zeus Marty!",
    long_description=open("README.md").read(),
    install_requires=[],
    extras_require={
        "test": ["pytest", "pytest-cov", "coverage"],
        "dev": ["black", "pylint", "watchdog"],
    },
    # Active la prise en compte du fichier MANIFEST.in
    include_package_data=True,
    # Une url qui pointe vers la page officielle de votre lib
    url="",
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers.
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: OSI Approved",
        "Natural Language :: French",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Education",
    ],
    # La syntaxe est "nom-de-commande-a-creer = package.module:fonction".
    entry_points={"console_scripts": ["nom-de-zeus = delorean.delorean:main"]},
)

